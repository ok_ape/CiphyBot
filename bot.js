/**
 * All necessary documentation related to discord.js and
 * discord.js-commando are here => https://discord.js.org
 * To get the token for your bot please refer to this following
 * link => https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token
 */

const commando = require('discord.js-commando')
const path = require('path')

const start = require('./start.js')

// Create new bot instance via commando.
const bot = new commando.Client({
	commandPrefix : 'bot ',
	unknownCommandResponse : false
})

bot.registry
	.registerGroups([
		['get', 'fetch reference links on topic'],
		['cmd', 'list of all available commands']
	])
	.registerDefaults()
    .registerCommandsIn(path.join(__dirname+'/commands'));

start(bot)

/**
 * All the events related to members in the guild, all
 * the methods can be found at /events/member_events.js
 */
	const MEMBER_EVENTS = require('./events/member_events.js')
	const member_events = new MEMBER_EVENTS(bot)
	member_events.watch_all()