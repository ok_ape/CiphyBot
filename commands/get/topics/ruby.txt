Reference Links for **Ruby**

Ruby documentation
<https://ruby-doc.org/>

Ruby for beginners
<http://rubylearning.com/>

Ruby programming Wiki book
<https://en.wikibooks.org/wiki/Ruby_programming_language>