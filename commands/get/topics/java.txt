Reference Links for **Java**

JavaJDK 9 documentation
<https://docs.oracle.com/javase/9/docs/api/overview-summary.html>

Oracle JavaSE official tutorial
<https://docs.oracle.com/javase/tutorial/>

Beginner friendly Java Tutorials from Javapoint
<https://www.javatpoint.com/java-tutorial>

Become conversant with Java programming language
<https://www.w3resource.com/java-tutorial/>

