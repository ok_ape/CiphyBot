const commando = require('discord.js-commando')
const fs = require('fs')
const path = require('path')

// -ref command is instanciated by extending it from commando.Command class
class Get_Command extends commando.Command{
	constructor(bot){
		super(bot, {
			name : 'get',
			group : 'get',
			memberName : 'get',
			description : 'fetch reference links on topic'
		})
	}

	// Following method returns name of files existing in topics folder
	get_topic_array(){
		let topic_list = []
		fs.readdirSync(path.join(__dirname, 'topics')).forEach(topic => {
			topic_list.push(topic.replace('.txt', ''))
		})
		return topic_list 
	}

	// Following method returns the content inside of file, whoes name is passed /
	// and is available in topics folder 
	get_topic_content(topic){
		let content = fs.readFileSync(path.join(__dirname, 'topics', `${topic}.txt`), 'utf8')
		return content 
	}

	// Following method creates a customised message in response to -ref topics /
	// command
	get_topics(){
		let msg = '*Things I know about*\n\n'
		this.get_topic_array().forEach(topic => {
			msg += ':white_check_mark:' + '`' + topic + '`\n\n'
		})
		return msg
	}

	// Following method creates a customised message in response to -ref help /
	// command
	get_help(){
		let help_text = '*You can use following commands*\n\n'
		help_text += '`-ref <topic_name>` To get reference links on topic\n\n'
		help_text += '`-ref topics` To get list of available topics\n\n'
		help_text += '`-ref help` To get help text for -ref command' 
		return help_text
	}

	async run(message, args){
		const arg_passed = args.split(' ')[0].toLowerCase()

		// Store all the files names available in topics folder
		let known_topics = []
		this.get_topic_array().forEach(topic => {
			known_topics.push(topic.toLowerCase())
		})

		// Initiate a response
		let response
		
		if(!arg_passed){
			
			// Response if no argument is passed by the sender
			response = 'Try `bot ' + this.name + '` to get help text for -ref command'
		}else{
			switch(arg_passed){
				case 'topics':
					response = this.get_topics()
					break
				case 'help':
					response = this.get_help()
					break
				default:
					if(known_topics.indexOf(arg_passed) == -1){
						response = 'Bummer! Found nothing on that. If you have a suggestion, let us know.'
					}else{
						response = this.get_topic_content(arg_passed)
					}
					break
			}
		}
		
		// Send response
		message.channel.send(response)
	}
}

module.exports = Get_Command