const bot_initials = require('./client.json')

// Method to start the bot
const start = client => {

	// Initialise the token with token in bot_initial.json or env variables
	let bot_token = false
	if(bot_initials){
		bot_token = bot_initials.token
	}
	const token = bot_token || process.env.BOT_TOKEN
	
	// Check wether the token is set or not
	if(token){
		console.log('bot login attempted...\nconnecting to discord api...')
		client.login(token).then(() => {
			console.log('bot successfully connected to discord')
		})
	}else{
		console.log('invalid bot token!\ncheck here => https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token')
	}	
}

/**
 * Calling method to start the bot, additional info 
 * regarding bot connection will be displayed in console.
 */

 module.exports = start